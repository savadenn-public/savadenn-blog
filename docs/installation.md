# Installation

## Dev

::: tip
This project use an external network to bind docker project to localhost.
See [this Ansible role](https://gitlab.com/savadenn-public/ansible-roles/local-dev/-/tree/stable/docker)
:::

1. Clone projet
2. Install submodules
    ```bash
    # checkout again
    git submodule update --init --recursive
    ```
3. Launch dev server
    ```
    make dev
    ```
   
::: warning
In case of error with submodules run 

```bash
git submodule deinit -f .
git submodule update --init --recursive
```
:::